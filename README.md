# book-searcher

## API Docs

http://0.0.0.0:8022/v1/docs

## setup

```shell
make start
```

## Setup [local]
```sh
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```

running tests:
```shell
make test
```
