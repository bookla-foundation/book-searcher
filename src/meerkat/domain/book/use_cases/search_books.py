from typing import List

from meerkat.domain.book.data_providers import BookDataProvider
from meerkat.domain.book.entities import Book


class SearchBooksUseCase:
    def __init__(self, book_data_provider: BookDataProvider):
        self.book_repository = book_data_provider

    def exec(self, query: str) -> List[Book]:
        books = self.book_repository.search(query)
        return books
