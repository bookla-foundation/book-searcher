from typing import List

from isbnlib import cover

from meerkat.domain.book.data_providers import BookDataProvider as AbstractBookDataProvider
import isbnlib

from meerkat.domain.book.entities import Book


class BookRepository(AbstractBookDataProvider):
    def search(self, query: str) -> List[Book]:
        results = isbnlib.goom(query)
        books = []
        for result in results:
            ISBN = result['ISBN-13']
            cover = isbnlib.cover(ISBN)
            description = isbnlib.desc(ISBN)
            books.append(
                Book(
                    isbn=ISBN, title=result['Title'], authors=result['Authors'],
                    publisher=result['Publisher'], year=int(result['Year']), language=result['Language'],
                    cover=cover, description=description
                )
            )
        return books

    def lookup(self, isbn: str) -> Book:
        pass
