from injector import Module, provider, singleton

from meerkat.domain.book.use_cases.search_books import SearchBooksUseCase
from meerkat.entrypoints.rest.book.resources import BookCollection


class BookConfigurator(Module):
    @singleton
    @provider
    def book_collection(self) -> BookCollection:
        return BookCollection(self.__injector__.get(SearchBooksUseCase))
