import json

import falcon

from meerkat.domain.book.use_cases.search_books import SearchBooksUseCase
from meerkat.entrypoints.rest.book.schemas import BookSchema


class BookCollection:
    def __init__(self, use_case: SearchBooksUseCase):
        self.use_case = use_case

    def on_get(self, req: falcon.Request, resp: falcon.Response):
        """Search books.
                ---
                parameters:
                    - name: q
                      in: query
                      required: true
                summary: Search Books
                description: Search Books
                responses:
                    200:
                        description: A list of books returned
                        schema: BookSchema
        """
        query_str = req.get_param(name='q', default='')
        # use case here
        books = []
        book_entities = self.use_case.exec(query_str)
        for book in book_entities:
            books.append(BookSchema.from_domain_object(book))

        resp.body = json.dumps(books)
        resp.content_type = "application/json"


