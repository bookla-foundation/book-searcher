import dataclasses
from typing import List

from meerkat.domain.common.types import Entity


@dataclasses.dataclass
class Book(Entity):
    isbn: str
    title: str
    authors: List
    publisher: str
    year: int
    language: str
    description: str
    cover: dict

    def small_thumbnail(self):
        return self.cover['smallThumbnail']

    def cover(self):
        return self.cover['thumbnail']

