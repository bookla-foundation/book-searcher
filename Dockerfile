FROM python:3.8.11-slim-buster

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y automake make git zsh util-linux && \
    rm -f /tmp/*

RUN pip install --upgrade pip pipenv==2020.11.15

# set working directory
RUN mkdir -p /code/
WORKDIR /code/

# add requirements
COPY Pipfile Pipfile.lock .env /code/

RUN pipenv install --system

# add entrypoint.sh
COPY ./.docker/entrypoint.sh /code/

EXPOSE 8000

# run server
CMD ["sh", "/code/entrypoint.sh"]
