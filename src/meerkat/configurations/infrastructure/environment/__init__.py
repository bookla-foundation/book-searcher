import os

from serviceregistry.services import BootableService, Container


class EnvironmentService(BootableService):
    def boot(self, container: Container):
        from meerkat.configurations.app.settings import Props

        container.set(Props.APP_URL, os.environ.get(Props.APP_URL.value))
