from meerkat.data_providers.isbnlib.repositories import BookRepository


class TestBookDataProvider:

    def test_search(self):
        data_provider = BookRepository()
        search_query = 'Apache Solr Search Patterns'
        results = data_provider.search(search_query)
        assert len(results) > 0
        first_match = results[0]
        assert first_match.title == 'Apache Solr Search Patterns'
        assert first_match.isbn == '9781783981854'
        assert first_match.year == 2015
        assert first_match.authors == ['Jayant Kumar']
        assert first_match.description != ''
        assert list(first_match.cover.keys()) == ['smallThumbnail', 'thumbnail']

