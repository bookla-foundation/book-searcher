import abc
from typing import List

from meerkat.domain.book.entities import Book


class BookDataProvider(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def search(self, query: str) -> List[Book]:
        """Search for books
        Args:
            query (string): query string to search through titles
        Return:
            List[dict]
        """
        pass


    @abc.abstractmethod
    def lookup(self, isbn: str) -> Book:
        """ Lookup a book by ISBN
        Args:
            isbn: accepts ISBN-13
        Return:
            Book
        """
        pass
