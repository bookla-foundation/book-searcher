from serviceregistry.services import Props as BaseProps

from meerkat.configurations.infrastructure.di.service import DiService
from meerkat.configurations.infrastructure.environment import EnvironmentService
from meerkat.configurations.infrastructure.logging import LoggingService
from meerkat.configurations.infrastructure.rest.health.registry import HealthService
from meerkat.configurations.infrastructure.rest.book.registry import BookService
from meerkat.configurations.infrastructure.rest.swagger.registry import SwaggerService

services = [
    LoggingService(),
    EnvironmentService(),
    DiService(),
    BookService(),
    HealthService(),
    SwaggerService(),
]


class Props(BaseProps):
    DI_PROVIDER = 0
    FALCON = 1

    APP_URL = "APP_URL"

