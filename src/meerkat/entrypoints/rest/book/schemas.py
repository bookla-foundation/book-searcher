from marshmallow import Schema, fields

from meerkat.domain.book.entities import Book


class BookSchema(Schema):
    class Meta:
        ordered = True

    id: fields.Str = fields.Str()
    title: fields.Str = fields.Str(required=True)
    isbn: fields.Str = fields.Str(required=True)
    year: fields.Str = fields.Str(required=True)
    language: fields.Str = fields.Str(required=True)
    description: fields.Str = fields.Str(required=True)
    cover: fields.Str = fields.Str(required=True)

    @classmethod
    def from_domain_object(cls, book: Book):
        object = cls()
        return object.load(
            {
                "title": str(book.title),
                "isbn": str(book.isbn),
                "year": str(book.year),
                "language": book.language,
                "description": book.description,
                "cover": book.small_thumbnail()
            }
        )