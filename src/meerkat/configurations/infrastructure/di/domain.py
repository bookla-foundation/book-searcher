from buslane.events import EventBus
from injector import singleton, provider, Module

from meerkat.data_providers.isbnlib.repositories import BookRepository
from meerkat.domain.book.use_cases.search_books import SearchBooksUseCase


class UseCasesConfigurator(Module):
    @singleton
    @provider
    def add_new(self) -> SearchBooksUseCase:
        return SearchBooksUseCase(
            self.__injector__.get(BookRepository)
        )
