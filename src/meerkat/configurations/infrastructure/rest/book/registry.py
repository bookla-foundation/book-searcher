from serviceregistry.services import BootableService, Container

from meerkat.configurations.app import settings

from meerkat.configurations.infrastructure.rest.book.definitions import BookConfigurator
from meerkat.entrypoints.rest.book.resources import BookCollection


class BookService(BootableService):
    def boot(self, container: Container):
        provider = container.get(settings.Props.DI_PROVIDER)
        provider.add_configurator(BookConfigurator)

    def post_boot(self, container):
        falcon = container.get(settings.Props.FALCON)
        provider = container.get(settings.Props.DI_PROVIDER)

        injector = provider.get_injector()

        falcon.add_route("/v1/books", injector.get(BookCollection))
